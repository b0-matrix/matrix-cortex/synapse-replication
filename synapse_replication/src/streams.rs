use std::collections::HashMap;
use std::fmt;

use serde_json::{json, Value};

use crate::edu_event::EduEvent;
use crate::presence_state::PresenceState;

#[derive(Debug, Clone)]
pub enum StreamRows {
    Event(EventStreamRow),
    Federation(FederationStreamRow),
    Unknown(String),
}

impl fmt::Display for StreamRows {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            StreamRows::Event(event) => event.fmt(f),
            StreamRows::Federation(federation) => federation.fmt(f),
            StreamRows::Unknown(unknown) => unknown.fmt(f),
        }
    }
}

pub trait StreamRow: fmt::Display {
    const NAME: &'static str;

    fn from_raw(str: String) -> Self;
}

// EventStreamRow at Synapse >=0.99.5
#[derive(Debug, Clone)]
pub enum EventStreamRow {
    Event(EventStreamRowEvent),
    State(EventStreamRowState),
}

impl StreamRow for EventStreamRow {
    const NAME: &'static str = "events";

    fn from_raw(data: String) -> EventStreamRow {
        let v: Value = serde_json::from_str(&data).unwrap();

        match v[0].as_str().unwrap() {
            "ev" => EventStreamRow::Event(EventStreamRowEvent::from_json(v[1].clone())),
            "state" => EventStreamRow::State(EventStreamRowState::from_json(v[1].clone())),
            _ => unimplemented!(),
        }
    }
}

impl fmt::Display for EventStreamRow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EventStreamRow::Event(event) => write!(f, "{}", event),
            EventStreamRow::State(state) => write!(f, "{}", state),
        }
    }
}

#[derive(Debug, Clone)]
pub struct EventStreamRowEvent {
    event_id: String,
    room_id: String,
    event_type: String,
    state_key: Option<String>,
    redacts: Option<String>,
    relates_to: Option<String>,
}

impl EventStreamRowEvent {
    fn from_json(data: Value) -> Self {
        let row = data.as_array().unwrap();

        Self {
            event_id: row[0].as_str().unwrap().to_string(),
            room_id: row[1].as_str().unwrap().to_string(),
            event_type: row[2].as_str().unwrap().to_string(),
            state_key: row[3].as_str().map(|s| s.to_string()),
            redacts: row[4].as_str().map(|s| s.to_string()),
            relates_to: row[5].as_str().map(|s| s.to_string()),
        }
    }
}

impl fmt::Display for EventStreamRowEvent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let rejson = json! {
            [self.event_id, self.room_id, self.event_type, self.state_key, self.redacts, self.relates_to]
        };

        write!(f, "{}", serde_json::to_string(&rejson).unwrap())
    }
}

#[derive(Debug, Clone)]
pub struct EventStreamRowState {
    event_id: String,
    room_id: String,
    event_type: String,
    state_key: String,
}

impl EventStreamRowState {
    fn from_json(data: Value) -> Self {
        let row = data.as_array().unwrap();

        Self {
            room_id: row[0].as_str().unwrap().to_string(),
            event_type: row[1].as_str().unwrap().to_string(),
            state_key: row[2].as_str().unwrap().to_string(),
            event_id: row[3].as_str().unwrap_or("").to_string(),
        }
    }
}

impl fmt::Display for EventStreamRowState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let rejson = json! {
            [self.room_id, self.event_type, self.state_key, self.event_id]
        };

        write!(f, "{}", serde_json::to_string(&rejson).unwrap())
    }
}

#[derive(Debug, Clone)]
pub enum FederationStreamRow {
    Keyed(FederationStreamRowKeyedEdu),
    Presence(FederationStreamRowPresence),
    Edu(FederationStreamRowEdu),
    Device(FederationStreamRowDevice),
    Other(String, Vec<Value>),
}

impl StreamRow for FederationStreamRow {
    const NAME: &'static str = "federation";

    fn from_raw(raw: String) -> Self {
        let data: Value = serde_json::from_str(&raw).unwrap();
        let mut row = data.as_array().unwrap();

        let type_id = row[0].as_str().unwrap();

        let mut row_clone = row.clone();
        row_clone.remove(0);

        match type_id {
            "k" => Self::Keyed(FederationStreamRowKeyedEdu::from_json(row_clone)),
            "p" => Self::Presence(FederationStreamRowPresence::from_json(row_clone)),
            "e" => Self::Edu(FederationStreamRowEdu::from_json(row_clone)),
            "d" => Self::Device(FederationStreamRowDevice::from_json(row_clone)),
            _ => Self::Other(type_id.to_string(), row_clone),
        }
    }
}

impl fmt::Display for FederationStreamRow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Clone)]
pub struct FederationStreamRowKeyedEdu {
    pub keyed_edus: HashMap<Vec<String>, EduEvent>,
}

impl FederationStreamRowKeyedEdu {
    fn from_json(array: Vec<Value>) -> Self {
        let mut keyed_edus: HashMap<Vec<String>, EduEvent> = HashMap::new();

        for json in array {
            let edu = EduEvent {
                edu_type: json["edu"]["edu_type"].as_str().unwrap().to_string(),
                internal_key: "".to_string(),
                stream_id: 0,
                content: json["edu"]["content"].clone(),
                origin: json["edu"]["origin"].as_str().unwrap().to_string(),
                destination: json["edu"]["destination"].as_str().unwrap().to_string(),
            };

            let key = json["key"]
                .as_array()
                .unwrap()
                .iter()
                .map(|s| s.as_str().unwrap().to_string())
                .collect();

            keyed_edus.insert(key, edu);
        }

        Self { keyed_edus }
    }
}

#[derive(Debug, Clone)]
pub struct FederationStreamRowPresence {
    pub presence: Vec<PresenceState>,
}

impl FederationStreamRowPresence {
    fn from_json(array: Vec<Value>) -> Self {
        let mut presence: Vec<PresenceState> = Vec::new();

        for json in array {
            let presence_state: PresenceState = serde_json::from_value(json).unwrap();
            presence.push(presence_state);
        }

        Self { presence }
    }
}

#[derive(Debug, Clone)]
pub struct FederationStreamRowEdu {
    pub edus: Vec<EduEvent>,
}

impl FederationStreamRowEdu {
    fn from_json(array: Vec<Value>) -> Self {
        let mut edus: Vec<EduEvent> = Vec::new();

        for json in array {
            let edu = EduEvent {
                content: json["content"].clone(),
                edu_type: json["edu_type"].as_str().unwrap().to_string(),
                internal_key: "".to_string(),
                stream_id: 0,
                origin: json["origin"].as_str().unwrap().to_string(),
                destination: json["destination"].as_str().unwrap().to_string(),
            };
            edus.push(edu);
        }

        Self { edus }
    }
}

#[derive(Debug, Clone)]
pub struct FederationStreamRowDevice {
    pub devices: Vec<String>,
}

impl FederationStreamRowDevice {
    fn from_json(array: Vec<Value>) -> Self {
        let mut devices: Vec<String> = Vec::new();

        for json in array {
            let destination = json["destination"].as_str().unwrap().to_string();
            devices.push(destination);
        }

        Self { devices }
    }
}
