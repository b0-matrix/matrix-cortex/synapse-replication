use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct EduEvent {
    pub content: Value,
    pub edu_type: String,

    #[serde(skip_serializing)]
    pub internal_key: String,
    #[serde(skip_serializing)]
    pub stream_id: u64,
    #[serde(skip_serializing)]
    pub origin: String,
    #[serde(skip_serializing)]
    pub destination: String,
}
