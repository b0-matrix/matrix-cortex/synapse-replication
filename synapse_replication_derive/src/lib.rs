extern crate proc_macro;
#[macro_use]
extern crate quote;

use proc_macro::TokenStream;

#[proc_macro_derive(CommandDisplay)]
pub fn derive_command_display(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();

    impl_command_display(&ast)
}

fn impl_command_display(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let gen = quote! {
        impl std::fmt::Display for #name {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(f, "{} {}", Self::NAME, self.to_line())
            }
        }
    };

    gen.into()
}
